# runarma2

Using this helper script is, or should be, easy

## Requirements
  * ~~g++~~
  * zenity
  * protontricks

## Usage
 0. Install arma2 and run it at least once (and see it fail)
 1. place the script somewhere and mark it as executable
 2. change the launch parameter of arma2 to be : `<scriptpath>/runarma2.sh %command%`
 3. copy your cdkey
 4. paste your cdkey in the entry box.

It should not ask for the key the next time. 

## Why ?
I wanted to play arma2 but for some reason it messes with the registry before launching.
This script should help get around that problem a little.


Hopefully have fun playing,

Ysblokje

