#!/bin/bash

###########################################################
# Using this helper script is easy
# Requirements : 
#   - g++
#   - zenity 
#   - protontricks
#
# 1) place the script somewhere and mark it as executable
# 2) change the launch parameter of 
#    arma2 to be : <scriptpath>/runarma2.sh %command%
# 3) copy your cdkey 
# 4) paste your cdkey in the entry box.
# 
# Ysblokje
###########################################################

REGFILE=~/.config/arma2.reg
CONFIGFILE=~/.config/arma2.config
ARMA2HEX=/tmp/arma2hex

echo reading arma cdkey
if [ -e ${CONFIGFILE} ]
then
    . ${CONFIGFILE}
fi

if [ "x${KEY}" == "x" ]
then
    KEY=`/usr/bin/zenity --text "Please enter your CDKEY" --entry`
    echo "KEY=${KEY}" >${CONFIGFILE}
fi

GAME_ID=${SteamAppId}

if [ ! -e "${REGFILE}" ]
then

NKEY=""
VALIDCHARS="0123456789ABCDEFGHJKLMNPRSTVWXYZ"
NR_VALIDCHAR=${#VALIDCHARS}

. "${HOME}/.config/arma2.config"

STRLEN=${#KEY}

#clean up the key
for ((i=0; i<STRLEN; i+=1))
do
    ITEM=${KEY:${i}:1}
    case $ITEM in
        O) NKEY="${NKEY}0"
            ;;
        I) NKEY="${NKEY}1" 
            ;;
        '-') 
            ;;
        ' ') 
            ;;
        *) NKEY="${NKEY}$ITEM"
        ;;
    esac
done

for ((i=0; i<15; i+=1))
do
    BYTEARRAY[$i]=0
done


for ((i=0; i<3; i+=1))
do
    bitwiseresult=0
    let i_eight=i\*8
    let i_five=i\*5
    for ((ii=0; ii<8; ii+=1))
    do
        let "key_it=i_eight+ii"
        vc_it=${NKEY:key_it:1}
        idx=0
        for ((j=0; j<NR_VALIDCHAR; j+=1))
        do
            if [ "$vc_it" = "${VALIDCHARS:j:1}" ]
            then
                break
            fi
            let "idx+=1"
        done

        let "idx<<=(ii*5)"
        let "bitwiseresult|=idx"
    done

    for ((ii=0; ii<5; ii+=1))
    do
        let "tmp=bitwiseresult&0xff"
        let "index=(i*5)+5-1-ii"
        byte=${BYTEARRAY:index:1}
        let "byte+=tmp"
        BYTEARRAY[index]=$byte
        let "bitwiseresult>>=8"
    done

done

FIRST=1
for i in ${BYTEARRAY[@]}
do
    if [ $FIRST -eq 1 ]
    then
        BYTEARRAY_RESULT="${BYTEARRAY_RESULT}$(printf '%02x' $i)"
        FIRST=0
    else
        BYTEARRAY_RESULT="${BYTEARRAY_RESULT}$(printf ',%02x' $i)"
    fi
done


cat <<EOF > $REGFILE
Windows Registry Editor Version 5.00

[HKEY_LOCAL_MACHINE\\Software\\Wow6432Node\\bohemia interactive studio\\arma 2]
"key"=hex:${BYTEARRAY_RESULT}
EOF

#echo ${BYTEARRAY_RESULT}
fi

echo importing registry key
protontricks -c "wine regedit /C ${REGFILE}" ${GAME_ID}

exec "$@"
